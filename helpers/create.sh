#!/bin/bash -x

kubectl apply -k apps/bootstrap/flux
kubectl apply -k apps/base
kubectl config view --raw --minify --flatten --context=dev  | kubectl create secret generic --context=control --namespace=dev-cluster dev-kubeconfig --from-file=value=/dev/stdin
kubectl config view --raw --minify --flatten --context=prod | kubectl create secret generic --context=control --namespace=prod-cluster prod-kubeconfig --from-file=value=/dev/stdin
